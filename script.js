function toggleBurguer(){

    const el = document.querySelector('.button-burguer');

    if(el.classList.contains('open')){
        el.classList.remove('open');
    }else{
        el.classList.add('open');
    }

}